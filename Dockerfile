FROM node:18-alpine AS runner
RUN apk add --no-cache libc6-compat
WORKDIR /app

COPY . .
RUN  npm install

ENV NEXT_TELEMETRY_DISABLED 1

RUN npm run build

ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

EXPOSE 4000

CMD ["npm", "start"]