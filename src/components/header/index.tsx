interface Props {
  className: string
}

export default function Header({ className } : Props) {
  return (
    <div 
      className={className + " flex flex-row flex-nowrap justify-between px-root md:px-mdroot bg-dark text-white text-xs font-medium"}
    >
      <div className="flex flex-row gap-x-2 h-12 items-center">
        <img src="/check_mark.png" />
        <span>
          30-DAY SATISFACTION GUARANTEE
        </span>
      </div>
      <div className="flex flex-row gap-x-2 items-center">
        <img src="/truck.png" />
        <span>
          FREE DELIVERY ON ORDERS OVER $40.00
        </span>
      </div>
      <div className="flex flex-row gap-x-2 items-center">
        <img src="/heart.png" />
        <span >
          50.000+ HAPPY CUSTOMERS
        </span>
      </div>
      <div className="flex flex-row gap-x-2 items-center">
        <img src="/fluent_arrow.png" />
        <span >
          100% MONEY BACK GUARANTEE
        </span>
      </div>
    </div>
  )
}