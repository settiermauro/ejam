//TODO: have to split Copyrigth span to fill the space in mobile
export default function Footer() {
  return (
    <div className="bg-dark text-white flex flex-row flex-wrap justify-center lg:justify-between gap-y-5 px-root md:px-mdroot py-6 w-full text-xs md:text-base">
      <span>
        Copyright (c) 2023 | clarifionsupport@clarifion.com
      </span>
      <div className="flex flex-row">
        <img className="px-1 object-contain" src='/lock.png' />
        <span className="text-nowrap">
          Secure 256-bit SSL encryption.
        </span>
      </div>
    </div>
  )
}