export default function StepsStripe(){
  return(
    <div className="flex flex-row justify-between text-xs">
      <div className="flex flex-col lg:flex-row lg:gap-x-3 items-center">
        <div className="w-6 h-6 lg:h-10 lg:w-10 rounded-full bg-darkgreen flex shrink-0 justify-center items-center">
          <img className="h-3.5 lg:h-4 object-contain" src="/white_tick.png" />
        </div>
        <div className="flex flex-row gap-x-2">
          <span className="hidden lg:block text-xl">
            Step 1 : 
          </span>
          <span className="lg:text-xl">
            Cart Review
          </span>
        </div>
      </div>
      <div className="flex flex-col lg:flex-row lg:gap-x-3 items-center">
        <div className="w-6 h-6 lg:h-10 lg:w-10 rounded-full bg-darkgreen flex shrink-0 justify-center items-center">
          <img className="h-3.5 lg:h-4 object-contain" src="/white_tick.png" />
        </div>
        <div className="flex flex-row gap-x-2">
          <span className="hidden lg:block text-xl">
            Step 2 :
          </span>
          <span className="lg:text-xl">
            Checkout
          </span>
        </div>
      </div>
      <div className="flex flex-col lg:flex-row lg:gap-x-3 items-center">
        <div className="w-6 h-6 lg:h-10 lg:w-10 rounded-full bg-blue flex shrink-0 justify-center items-center">
          <span className="text-white text-sm lg:text-xl">
            3
          </span>
        </div>
        <div className="flex flex-row gap-x-2">
          <span className="hidden lg:block text-xl">
            Step 3 :
          </span>
          <span className="font-bold lg:text-xl">
            Special Offer
          </span>
        </div>
      </div>
      <div className="flex flex-col lg:flex-row lg:gap-x-3 items-center">
        <div className="w-6 h-6 lg:h-10 lg:w-10 rounded-full border-2 border-blue flex shrink-0 justify-center items-center">
          <span className="text-blue text-sm lg:text-xl">
            4
          </span>
        </div>
        <div className="flex flex-row gap-x-2">
          <span className="hidden lg:block text-xl">
            Step 4 :
          </span>
          <span className="lg:text-xl">
            Confirmation
          </span>
        </div>
      </div>
    </div>
    )}