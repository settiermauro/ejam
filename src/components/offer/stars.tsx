interface Props {
  num: number
}

const stars = (num : number) => {
  const elements = []
  for(let i=0; i<num; i++){
    elements.push(<img key={i} src="/star.png"/>)
  }
  return elements
}

export default function Stars({num} : Props){
  return(
    <div className="flex flex-row">
      {stars(num)}
    </div>
  )
}