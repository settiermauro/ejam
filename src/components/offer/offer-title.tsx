interface Props {
  innerHtml: string,
  className: string,
  testprefix: string
}

export default function OfferTitle({innerHtml, className, testprefix} : Props) {
  return(<p 
        className={"text-2xl md:text-3xl " + className}
        data-testid={testprefix + "-lg-description"}
        dangerouslySetInnerHTML={{__html: innerHtml}}
      />)
}