import { IReview } from '@api/interfaces'
import Stars from './stars'

interface Props {
  review: IReview,
  className: string
}

export default function Review({review, className} : Props) {
  return(
    <div className={className + " bg-white flex flex-col p-6 gap-y-4"}>
      <div className="flex flex-row gap-x-2">
        <img src="/user_image.png" />
        <div className="flex flex-col justify-around">
          <Stars
            num={review.stars}
          />
          <div className="flex flex-row gap-x-2">
            <span className="text-nowrap txt-darkestgray font-bold text-sm">
              {review.user_name}
            </span>
            <img className="object-contain" src="/verify.png" />
            <span className="text-green text-nowrap text-sm">
              Verified Customer
            </span>
          </div>
        </div>
      </div>
      <span className="text-darkergray text-base">
        {review.description}
      </span>
    </div>
  )
}