import { IOffer } from '@api/interfaces'
import OfferContent from './offer-content'

interface Props {
  offer?: IOffer
}

export default function Offer({offer} : Props) {
  if(offer == undefined){
    return(
      <div>
      </div>
    )
  }
  return (
    <OfferContent
      offer={offer}
    />
  )
}