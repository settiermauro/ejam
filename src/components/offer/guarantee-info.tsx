export default function GuranteeInfo(){
  return(
    <div className="flex flex-row gap-x-4 items-start">
      <img className="object-contain h-12 lg:h-20" src="/satisfaction.png" />
      <p className="text-xs lg:text-base text-darkergray">
        If you are not completely thrilled with your Clarifion - We have a <span className="font-bold">30 day satisfaction guarantee</span>. Please refer to our return policy at the bottom of the page for more details. Happy Shopping!
      </p>
    </div>
  )
}