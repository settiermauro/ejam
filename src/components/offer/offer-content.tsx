import { IOffer } from '@api/interfaces'
import OfferTitle from './offer-title'
import GuaranteeInfo from './guarantee-info'
import PaymentInfo from './payment-info'
import Stars from './stars'
import Review from './review'

interface Props {
  offer: IOffer
}

export default function OfferConten({offer} : Props) {
  return (
    <div className="flex flex-col lg:flex-row lg:bg-ligthgray lg:p-8  gap-y-4 lg:gap-x-8">
      <div className="flex flex-col w1/2 basis-1/2 gap-y-4">
        <OfferTitle
          testprefix="sm"
          className="lg:hidden"
          innerHtml={offer.description_html}
        />
        <img
          aria-label="product image"
          src="/product.png"
        />
        <Review
          className="hidden lg:block"
          review={offer.product.reviews[0]}
        />
      </div>
      <div className="flex flex-col gap-y-4 w1/2 basis-1/2">      
        <OfferTitle
          testprefix="lg"
          className="lg:block hidden"
          innerHtml={offer.description_html}
        />
        <div className="flex flex-row w-full h-20 lg:h-36">
          <img
            className="bg-blue rounded-xl"
            aria-label="product preview"
            src="/product_preview.png"
          />
          <div className="flex flex-row px-4 py-2 w-full justify-between">
            <div className="flex flex-col h-full justify-between">
              <div className="flex flex-row justify-between">
                <span className="text-sm lg:text-xl">
                  {offer.product.name}
                </span>
                <div className="flex flex-row gap-x-2">
                  <span className="text-gray text-xs line-through">
                    {"$" +offer.product.price}
                  </span>
                  <span className="text-blue font-semibold text-sm">
                    {"$" +offer.price}
                  </span>
                </div>
              </div>
              <Stars
                num={offer.product.stars}
              />
              <div className="flex flex-row gap-x-4 items-center text-darkgray text-sm font-light">
                <img className="h-3 object-contain" src="/radio.png" />
                <span>
                  {offer.product.stock + " left in Stock"}
                </span>
              </div>
              <span className="text-darkergray text-sm text-center hidden lg:block">
                Simply plug a Clarifion into any standard outlet and replace bulky, expensive air purifiers with a simple.
              </span>
            </div>
          </div>
        </div>
        <span className="text-darkergray text-sm text-center lg:hidden">
          Simply plug a Clarifion into any standard outlet and replace bulky, expensive air purifiers with a simple.
        </span>
        <p 
          data-testid="features-description"
          dangerouslySetInnerHTML={{__html: offer.product.features_html}}
        />
        <p 
          data-testid="short-description"
          dangerouslySetInnerHTML={{__html: offer.description_short_html}}
        />
        <button className="bg-darkgreen rounded-full flex flex-row justify-center py-4 gap-4">
          <span className="text-white text-sm lg:text-xl font-bold">
            {"Yes - Claim my discount".toUpperCase()}
          </span>
          <img className="w-3 lg:w-4 object-contain" src="/arrow.png" />
        </button>
        <PaymentInfo />
        <button>
          <span className="text-red text-xs underline lg:text-lg font-medium lg:font-semibold">
            {"No thanks, I don’t want this.".toUpperCase()}
          </span>
        </button>
        <GuaranteeInfo />
      </div>
    </div>
  )
}