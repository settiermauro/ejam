export default function PaymentInfo(){
  return(
    <div className="flex flex-col xl:flex-row border rounded border-bordergray py-2 px-4 gap-3">
      <div className="flex flex-row justify-between text-xs text-darkergray xl:grow">
        <span>
          Free shipping
        </span>
        <span className="text-bordergray">
          |
        </span>
        <div className="flex flex-row gap-x-2">
          <img className="w-3 object-contain" src='/dark_lock.png' />
          <span className="text-nowrap">
            Secure 256-bit SSL encryption.
          </span>
        </div>
        <span className="text-bordergray hidden xl:block">
          |
        </span>
      </div>
      <div className="flex flex-row justify-center pt-2 gap-0.5 border-t border-bordergray xl:border-0 xl:p-0">
        <img className="w-6 object-contain" src='/visa.png' />
        <img className="w-6 object-contain" src='/shop_pay.png' />
        <img className="w-6 object-contain" src='/paypal.png' />
        <img className="w-6 object-contain" src='/mastercard.png' />
        <img className="w-6 object-contain" src='/gpay.png' />
        <img className="w-6 object-contain" src='/apple_pay.png' />
        <img className="w-6 object-contain" src='/amex.png' />
      </div>
    </div>
  )
}