import { Carousel } from '@material-tailwind/react'

interface Props {
  className: string
}

export default function HeaderCarousel({ className } : Props) {
  return (
    <Carousel 
      className={className + " py-2.5 bg-dark text-white h-11 w-full text-xs font-medium"}
      navigation={() => {}}
    >
      <div className="flex flex-row justify-center w-full h-full gap-x-2 items-center">
        <img src="/check_mark.png" />
        <span >
          30-DAY SATISFACTION GUARANTEE
        </span>
      </div>
      <div className="flex flex-row justify-center w-full h-full gap-x-2 items-center">
        <img src="/truck.png" />
        <span >
          FREE DELIVERY ON ORDERS OVER $40.00
        </span>
      </div>
      <div className="flex flex-row justify-center w-full h-full gap-x-2 items-center">
        <img src="/heart.png" />
        <span >
          50.000+ HAPPY CUSTOMERS
        </span>
      </div>
      <div className="flex flex-row justify-center w-full h-full gap-x-2 items-center">
        <img src="/fluent_arrow.png" />
        <span >
          100% MONEY BACK GUARANTEE
        </span>
      </div>
    </Carousel>
  )
}