'use client'
import { useEffect, useState } from 'react'
import Footer from '@components/footer'
import HeaderCarousel from '@components/header-carousel'
import Header from '@components/header'
import SetpsStripe from '@components/steps-stripe'
import Offer from '@components/offer'
import { getOffer } from '@api/offer'
import { IOffer } from '@api/interfaces'

export default function Home() {

  const [offer, setOffer] = useState<IOffer>()

  useEffect(() => {
    getOffer().then(result => setOffer(result))
  }, [])

  return (
    <div className="flex flex-col h-screen justify-between">
      <div>
        <Header className="hidden lg:flex" />
        <HeaderCarousel className="lg:hidden" />
        <div className="px-root md:px-mdroot flex flex-col pb-4 gap-y-4 lg:gap-y-8">
          <div className="flex flex-row justify-between py-5">
            <img className="object-contain h-5 sm:h-9" src="/clarifon.png" />
            <div className="flex flex-row gap-x-4">
              <img className="object-contain h-4 sm:h-8" src="/mcafee.png" />
              <img className="object-contain h-4 sm:h-8" src="/norton.png" />
            </div>
          </div>
          <span className="text-3xl text-center lg:text-5xl">
            Wait ! your order in progress.
          </span>
          <span className="text-xs text-darkgray text-center lg:text-2xl">
            Lorem Ipsum Dolor Sit Amet, Consectetur 
          </span>
          <SetpsStripe />
          <Offer
            offer={offer}
          />
        </div>
      </div>
      <Footer />
    </div>
  )
}
