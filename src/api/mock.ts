const review = {
  user_name: "Ken T.",
  user_image:"/user_image.png",
  description: "“As soon as the Clarifions arrived I put one in my bedroom. This was late in the afternoon. When I went to the bedroom in the evening it smelled clean. When I went to bed I felt I could breathe better. Wonderful.”",
  stars: 5
}

const product = {
  name: "Clarifion Air Ionizer",
  product_image:"/product.png",
  product_preview_image:"/product_preview.png",
  description: "Simply plug a Clarifion into any standard outlet and replace bulky, expensive air purifiers with a simple.",
  features_html: "<div class=\"flex flex-col gap-y-4 text-darkergray text-sm\"><ul class=\"list-image-[url(/tick.png)] list-inside\"><li class=\"pb-4\">Negative Ion Technology may <span class=\"font-bold\">help with allergens</span></li><li class=\"pb-4\">Designed for <span class=\"font-bold\">air rejuvenation</span></li><li><span class=\"font-bold\">Perfect for every room</span> in all types of places.</li></ul></div>",
  price: 180,
  reviews: [review],
  stars: review.stars,
  stock: 12
}

const offer = {
  product: product,
  description_html: "<p><span class=\"text-blue\">ONE TIME ONLY</span> Special Price For 6 Extra Clarifion For Only <span class=\"text-blue\">$14 Each</span> ($84.00 Total!)</p>",
  description_short_html: "<div class=\"bg-cyanblue rounded-xl py-3 px-4 text-sm flex flex-row items-center gap-x-4\"><div class=\"w-6 h-6 rounded-full bg-blue flex shrink-0 justify-center items-center\"><img class=\"h-3.5 object-contain\" src=\"/percent.png\"></div><p>Save <span class=\"text-blue\">53%</span> and get <span class=\"text-blue\">6 extra Clarifision</span> for only <span class=\"text-blue\">$14 Each</span>.</p></div>",
  price: 84
}

export default offer