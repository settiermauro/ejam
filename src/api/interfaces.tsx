export interface IReview {
  user_name: string,
  user_image: string,
  description: string,
  stars: number
}

export interface IProduct {
  name: string,
  product_image: string,
  product_preview_image: string,
  description: string,
  features_html: string,
  price: number,
  reviews: IReview[],
  stars: number,
  stock: number
}

export interface IOffer {
  product: IProduct,
  description_html: string,
  description_short_html: string,
  price: number
}