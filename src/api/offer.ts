import fakeData from '@api/mock'
import { IOffer } from '@api/interfaces'

const getOffer = () => {
  return new Promise<IOffer>(resolve => {
    resolve(fakeData)
  })
}

export {
  getOffer
}