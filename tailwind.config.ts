import type { Config } from "tailwindcss"
const withMT = require("@material-tailwind/react/utils/withMT")

const config: Config = withMT({
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/api/mock.ts",
  ],
  theme: {
    extend: {
      spacing: {
        mdroot: '10%',
        root: '5%'
      },
      fontFamily: {
        manrope: 'Manrope',
      },
      colors:{
        dark: "#252F3D",
        white: "#ffffff",
        blue: "#2C7EF8",
        gray: "#969696",
        ligthgray: "#FAFAFA",
        darkgray: "#37465A",
        darkergray: "#4D5254",
        darkestgray: "#333333",
        bordergray: "#CFCFCF",
        cyanblue: "#EDF3FD",
        green: "#5BB59A",
        darkgreen: "#59AE43",
        red: "#F82C2C"
      },
    },
  },
  plugins: [],
})
export default config
