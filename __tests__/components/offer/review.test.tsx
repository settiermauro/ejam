import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Review from '@components/offer/review'
import fakeData from '@api/mock'
import renderer from 'react-test-renderer'
 
describe('Review', () => {
  const review = fakeData.product.reviews[0]
  it('renders the Review with all elements', () => {
    render(
      <Review
        review={review}
      />)
    expect(screen.getAllByRole('img')).toHaveLength(7)
    expect(screen.getByText(review.user_name)).toBeInTheDocument()
    expect(screen.getByText(review.description)).toBeInTheDocument()
  })

  it('renders correctly', () => {
    const tree = renderer
      .create(
      <Review
        review={review}
      />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  })
})