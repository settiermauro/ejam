import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Offer from '@components/offer'
import fakeData from '@api/mock'
import renderer from 'react-test-renderer'
 
describe('Offer', () => {
  it('renders the offer with all elements', () => {
    render(
      <Offer
        offer={fakeData}
      />)
    expect(screen.getByTestId('sm-lg-description').innerHTML).toMatch(fakeData.description_html)
    expect(screen.getByTestId('lg-lg-description').innerHTML).toMatch(fakeData.description_html)
    expect(screen.getByRole('img', {name: 'product image'})).toBeInTheDocument()
    expect(screen.getByRole('img', {name: 'product preview'})).toBeInTheDocument()
    expect(screen.getByText(fakeData.product.name)).toBeInTheDocument()
    expect(screen.getByText("$" + fakeData.price)).toBeInTheDocument()
    expect(screen.getByText("$" + fakeData.product.price)).toBeInTheDocument()
    expect(screen.getByText(fakeData.product.stock + " left in Stock")).toBeInTheDocument()
    expect(screen.getAllByText(fakeData.product.description)).toHaveLength(2)
    expect(screen.getByTestId('features-description').innerHTML).toMatch(fakeData.product.features_html)
    expect(screen.getByTestId('short-description').innerHTML).toMatch(fakeData.description_short_html)
  })

  it('renders correctly', () => {
    const tree = renderer
      .create(<Offer
        offer={fakeData}
      />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  })
})