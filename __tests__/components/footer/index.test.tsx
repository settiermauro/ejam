import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import renderer from 'react-test-renderer'
import Footer from '@components/footer'
 
describe('Footer', () => {
  it('renders the footer with all elements', () => {
    render(<Footer />)
    expect(screen.getByText('Copyright (c) 2023 | clarifionsupport@clarifion.com')).toBeInTheDocument()
    expect(screen.getByText('Secure 256-bit SSL encryption.')).toBeInTheDocument()
    expect(screen.getByRole('img')).toBeInTheDocument()
  })

  it('renders correctly', () => {
    const tree = renderer
      .create(<Footer />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  })
})