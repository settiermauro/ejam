import '@testing-library/jest-dom'
import { getOffer } from '@api/offer'
import fakeData from '@api/mock'


 
describe('Offer api', () => {
  it('get offer', () => {
    return getOffer().then(result => {
      expect(result).toMatchObject(fakeData)
    })
  })
})